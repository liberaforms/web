export const TEXT_FIELD = {
  type: 'input',
  inputType: 'text'
}
export const EMAIL_FIELD = {
  type: 'input',
  inputType: 'email'
}
export const TEL_FIELD = {
  type: 'input',
  inputType: 'tel'
}
export const NUMBER_FIELD = {
  type: 'input',
  inputType: 'number'
}
