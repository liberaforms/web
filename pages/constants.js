export const MENU_ITEMS = [
  {
    href: '/users',
    title: 'app.users',
    groups: ['admin']
  },
  {
    href: '/invites',
    title: 'app.invites',
    groups: ['admin']
  },
  {
    href: '/dashboard',
    title: 'app.dashboard',
    groups: ['user']
  },
  {
    href: '/forms',
    title: 'app.forms',
    groups: ['admin', 'user']
  }
]
