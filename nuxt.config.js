/* eslint-disable nuxt/no-cjs-in-config */
const EN_LOCALE = require('./locales/en.json')
const ES_LOCALE = require('./locales/es.json')
const FR_LOCALE = require('./locales/fr.json')
const CA_LOCALE = require('./locales/ca.json')
const API_BASE_URL = process.env.API_BASE_URL || 'http://localhost:5000/api'

export default {

  // Target (https://go.nuxtjs.dev/config-target)
  target: 'static',

  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: 'liberaforms-client',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: ['~/assets/styles/global.css'],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: ['plugins/vue-form-generator'],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module'
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    'bootstrap-vue/nuxt',
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://auth.nuxtjs.org
    '@nuxtjs/auth',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    // https://go.nuxtjs.dev/content
    '@nuxt/content',
    // https://i18n.nuxtjs.org
    'nuxt-i18n'
  ],

  i18n: {
    locales: [
      {
        code: 'en',
        name: 'English'
      },
      {
        code: 'fr',
        name: 'Français'
      },
      {
        code: 'es',
        name: 'Español'
      },
      {
        code: 'ca',
        name: 'Català'
      }
    ],
    defaultLocale: 'en',
    vueI18n: {
      fallbackLocale: 'en',
      messages: {
        en: EN_LOCALE,
        es: ES_LOCALE,
        fr: FR_LOCALE,
        ca: CA_LOCALE
      }
    }
  },

  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {},

  // Auth module configuration https://auth.nuxtjs.org/schemes
  auth: {
    redirect: {
      login: '/auth/login'
    },
    strategies: {
      local: {
        scheme: 'refresh',
        token: {
          property: 'access_token'
          // maxAge: 1800
          // type: 'Bearer'
        },
        refreshToken: {
          property: 'refresh_token'
          // data: 'refresh_token',
          // maxAge: 60 * 60 * 24 * 30
        },
        endpoints: {
          login: {
            url: `${API_BASE_URL}/auth/login`,
            method: 'post',
            propertyName: 'access_token'
          },
          user: {
            url: `${API_BASE_URL}/users/me`,
            method: 'get',
            propertyName: false
          },
          refresh: {
            url: `${API_BASE_URL}/auth/refresh`,
            method: 'post'
          },
          logout: false
        }
      }
    },
    plugins: [{ src: '~/plugins/api', ssr: true }]
  },

  router: {
    middleware: ['auth']
  },

  // Content module configuration (https://go.nuxtjs.dev/config-content)
  content: {},

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {},

  env: {
    API_BASE_URL
  }
}
